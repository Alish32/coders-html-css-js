import React, { Component } from 'react';
import './App.css';

import Input from './Input/Input'
import Tasks from './Tasks/Tasks'

class App extends Component {

  state = {
    gorevler: ['Homework', 'Reading']
  }


  taskDeleteHandler = (index) => {

    const myArray = [...this.state.gorevler] // ['Homework', 'Reading']
    myArray.splice(index, 1) // ['Reading']

    this.setState({
      gorevler: myArray
    })
  }

  taskAddHandler = (input) => {
    // const task = document.getElementById('my-input').value
    const task = input.current.value

    const updatedArray = [...this.state.gorevler]
    updatedArray.push(task)

    this.setState({
      gorevler: updatedArray
    })

  }

  render() {

    const gorevComponents = this.state.gorevler.map((gorev, index) => {
      return <Tasks taskName={gorev} key={index} deleted={() => this.taskDeleteHandler(index)} />
    })

    return (
      <div className="App">
        <Input added={(event) => this.taskAddHandler(event)}/>
        <div>
          {gorevComponents}
        </div>

      </div>
    );
  }
}

export default App;
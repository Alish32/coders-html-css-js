import React from 'react'
import './Tasks.css'
const Tasks = (props) => {
    return (
        <div className="Task">
            <p>{props.taskName}</p>
            <button onClick={props.deleted}>Completed</button>
        </div>
    )
}

export default Tasks
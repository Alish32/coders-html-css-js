import React from 'react'

const Input = (props) => {

    const textInput = React.createRef()

    return (
        <div>
            <input ref={textInput} type="text" id="my-input" />
            <button onClick={() => props.added(textInput)}>Add</button>
        </div>
    )
}

export default Input
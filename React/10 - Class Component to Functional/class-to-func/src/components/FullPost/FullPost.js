import React, { useState, useEffect } from 'react';
import axios from 'axios'
import './FullPost.css';

const FullPost = (props) => {
    const [loadedPost, setLoadedPost] = useState(null)

    useEffect(() => {
        console.log('DID MOUNT');
    }, [])
    
    useEffect(() => {
        if (props.id != null) {
            axios.get(`http://jsonplaceholder.typicode.com/posts/${props.id}`)
                .then(response => {
                    // handle success
                    console.log(response.data);
                    setLoadedPost(response.data)
                })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                })
                .then(function () {
                    // always executed
                });
        }
    }, [props.id])

    let post = <p>Please select a Post! {props.id}</p>;

    if (props.id != null && loadedPost != null) {
        post = (
            <div className="FullPost">
                <h1>{props.id}</h1>
                <h1>{loadedPost.title}</h1>
                <p>{loadedPost.body}</p>
                <div className="Edit">
                    <button className="Delete" onClick={() => props.deleted(loadedPost.id)}>Delete</button>
                </div>
            </div>
        );
    }

    return post;

}

export default FullPost;
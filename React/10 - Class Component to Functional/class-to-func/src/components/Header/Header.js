import React from 'react'
import { NavLink } from 'react-router-dom'
import './Header.css'
const Header = () => {
    return (
        <header className="Header">
            <ul>
                <li><NavLink to={{
                    pathname: "/",
                    search: "?id=1",
                }}
                    exact activeStyle={
                        {
                            color: 'yellow'
                        }
                    }>Home</NavLink></li>
                <li><NavLink to="/about" activeClassName="my-active">About</NavLink></li>
            </ul>
        </header>
    )
}

export default Header
import React, { useEffect } from 'react'

const About = (props) => {

    useEffect(() => {
        console.log('About.js --- useEffet');
        console.log(props);
    })

    return (
        <h2>About Page</h2>
    )
}

export default About
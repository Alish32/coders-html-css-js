import React, { Component } from 'react';

import Post from '../../components/Post/Post';
import FullPost from '../../components/FullPost/FullPost';
import './Blog.css';
// import { useHistory } from "react-router-dom";

import axios from 'axios'

class Blog extends Component {

    state = {
        posts: [],
        selectedId: null
    }

    componentDidMount() {
        axios.get('http://jsonplaceholder.typicode.com/posts')
            .then(response => {
                // handle success
                const data = response.data
                this.setState({
                    posts: data
                })
                // const history = useHistory();
                // history.push("/about");

                // this.props.history.push('/dashboard')

            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })
            .then(function () {
                // always executed
            });
    }

    deletePostHanler = (id) => {
        console.log(id);
        const posts = [...this.state.posts]
        const index = posts.findIndex(post => {
            return post.id === id
        })

        posts.splice(index, 1)

        this.setState({
            posts: posts,
            selectedId: null
        })

    }
    render() {
        const posts = this.state.posts.map(post => {
            return <Post key={post.id}
                title={post.title}
                id={post.id}
                clicked={(id) => { this.setState({ selectedId: id }) }} />
        })
        return (
            <div>
                <section className="Posts">
                    {posts}
                </section>
                <section>
                    <FullPost id={this.state.selectedId} deleted={this.deletePostHanler} />
                </section>
            </div>
        );
    }
}

export default Blog;
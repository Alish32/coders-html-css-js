import './App.css';
import Blog from './containers/Blog/Blog'
import About from './containers/About/About'
import Header from './components/Header/Header'

import { Redirect, Route, Switch } from 'react-router-dom'

function App() {
  const loggedIn = true
  return (
    <div className="App">
      <Header />

      <Switch>
        <Route path="/" exact component={Blog} />
        <Route path="/about" exact> {!loggedIn ? <Redirect to="/" /> : <About />} </Route>
        <Route path="/about/:id" > <h1>nsajsakn</h1></Route>
        <Route path="*">
          <h1>NotFound</h1>
        </Route>
      </Switch>
    </div>
  );
}

export default App;

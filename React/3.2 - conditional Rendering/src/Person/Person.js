import React from 'react'

const person = props => {
    return (
        <div>
            <p onClick={props.kliklendi}>I am {props.name} and I am {props.age} years old</p>
            <p>Hobby: {props.children}</p>
            <input type="text" onChange={props.changed} value={props.name} />
        </div>
    )
}

export default person

//state less companent - dummy component - presentational component
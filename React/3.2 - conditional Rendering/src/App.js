import React, { Component } from 'react';
import './App.css';
import Person from './Person/Person'

class App extends Component {

  state = {
    persons: [
      { name: 'Alish', age: "28" },
      { name: 'Fariz', age: "27" },
      { name: 'Meqsed', age: "23" },
    ],
    digerState: 'Diger state text',
    showPerson: true
  }
  switchNameHandler = (newName) => {
    console.log('Clikcledddd');
    this.setState({
      persons: [
        { name: newName, age: "28" },
        { name: 'Fariz', age: "27" },
        { name: 'Meqsed', age: "23" },
      ]
    })
  }

  nameChangeInputHandler = (event) => {
    this.setState({
      persons: [
        { name: event.target.value, age: "28" },
        { name: 'Fariz', age: "27" },
        { name: 'Meqsed', age: "23" },
      ]
    })
  }

  togglePersonHandler = () => {
    this.setState({
      showPerson: !this.state.showPerson
    })
  }
  render() {

    let persons = null

    if (this.state.showPerson) {
      persons = (
        <div>
          <Person
            changed={this.nameChangeInputHandler}
            kliklendi={this.switchNameHandler.bind(this, 'Alish new')}
            name={this.state.persons[0].name}
            age={this.state.persons[0].age} />
          <Person
            name={this.state.persons[1].name}
            age={this.state.persons[1].age} >My hobbies: swimming</Person>
          <Person
            name={this.state.persons[2].name}
            age={this.state.persons[2].age} />

        </div>
      )
    }

    return (
      <div className="App">
        <h2>Hi</h2>
        <button onClick={this.togglePersonHandler}>Toggle Person</button>
        {persons}
      </div>
    );
  }
}

export default App;

// statefull component, main component
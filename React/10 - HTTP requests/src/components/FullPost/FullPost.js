import React, { Component } from 'react';
import axios from 'axios'
import './FullPost.css';

class FullPost extends Component {
    state = {
        loadedPost: null
    }
    componentDidUpdate() {
        if (this.props.id != null) {

            if (!this.state.loadedPost || (this.state.loadedPost && this.state.loadedPost.id !== this.props.id)) {
                axios.get(`http://jsonplaceholder.typicode.com/posts/${this.props.id}`)
                    .then(response => {
                        // handle success
                        console.log(response.data);
                        this.setState({
                            loadedPost: response.data
                        })
                    })
                    .catch(function (error) {
                        // handle error
                        console.log(error);
                    })
                    .then(function () {
                        // always executed
                    });
            }
        }
    }
    render() {
        let post = <p>Please select a Post!</p>;

        if (this.props.id != null && this.state.loadedPost != null) {
            post = (
                <div className="FullPost">
                    <h1>{this.props.id}</h1>
                    <h1>{this.state.loadedPost.title}</h1>
                    <p>{this.state.loadedPost.body}</p>
                    <div className="Edit">
                        <button className="Delete" onClick={() => this.props.deleted(this.state.loadedPost.id)}>Delete</button>
                    </div>
                </div>
            );
        }

        return post;
    }
}

export default FullPost;
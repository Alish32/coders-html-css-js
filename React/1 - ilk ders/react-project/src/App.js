import React, { Component } from 'react';
import './App.css';
import Person from './Person/Person'

class App extends Component {
  render() {
    return (
      <div className="App">
        <h2>Hi</h2>
        <Person name="Alish" age="28"/>
        <Person name="Fariz" age="27" >My hobbies: swimming</Person>
        <Person name="Meqsed" age="23"/>
      </div>
    );

  // return React.createElement('div', {className: 'App'}, React.createElement('h2', null, 'Hi'))  
  }
}

export default App;

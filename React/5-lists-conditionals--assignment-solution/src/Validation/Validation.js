import React from 'react'

const validation = props => {
    let messageText = 'Text long enough'

    if(props.uzunluq <=5) {
        messageText = 'Text too short'
    }

    return (
        <div>
            <p>{messageText}</p>
        </div>
    )
}

export default validation
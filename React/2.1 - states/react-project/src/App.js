import React, { Component } from 'react';
import './App.css';
import Person from './Person/Person'

class App extends Component {

  state = {
    persons: [
      { name: 'Alish', age: "28" },
      { name: 'Fariz', age: "27" },
      { name: 'Meqsed', age: "23" },
    ],
    digerState: 'Diger state text'
  }

  adiDeyisenHandler = () => {
    console.log('Clicked');
    // this.state.persons[1].name = 'Fariz new'
    this.setState({
      persons: [
        { name: 'Alish', age: "28" },
        { name: 'Fariz new', age: "27" },
        { name: 'Meqsed', age: "23" },
      ]
    })
  }

  switchNameHandler = (newName) => {
    console.log('Clikcledddd');
    this.setState({
      persons: [
        { name: newName, age: "28" },
        { name: 'Fariz', age: "27" },
        { name: 'Meqsed', age: "23" },
      ]
    })
  }

  nameChangeInputHandler = (event) => {
    this.setState({
      persons: [
        { name: event.target.value, age: "28" },
        { name: 'Fariz', age: "27" },
        { name: 'Meqsed', age: "23" },
      ]
    })
  }

  render() {
    console.log(this.state);
    return (
      <div className="App">
        <h2>Hi</h2>
        <button onClick={this.adiDeyisenHandler}>Switch Name</button>

        <Person 
        changed={this.nameChangeInputHandler}
        kliklendi={this.switchNameHandler.bind(this, 'Alish new')}
        name={this.state.persons[0].name}
        age={this.state.persons[0].age} />
        <Person 
        name={this.state.persons[1].name}
        age={this.state.persons[1].age} >My hobbies: swimming</Person>
        <Person 
        name={this.state.persons[2].name} 
        age={this.state.persons[2].age} />
      </div>
    );
  }
}

export default App;

// statefull component, main component
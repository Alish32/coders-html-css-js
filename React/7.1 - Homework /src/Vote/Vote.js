import React from 'react'
import './Vote.css'

const Vote = (props) => {
    return (
        <div className="Vote">
            <span>{props.VoteCount}</span>
            <span>{props.name}</span>
            <span className="click-button" onClick={props.clicked}>Click Here</span>
        </div>
    )
}

export default Vote
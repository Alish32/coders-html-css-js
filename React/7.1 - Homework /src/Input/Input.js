import React from 'react'
// import './Vote.css'

const Input = (props) => {
    const input = React.useRef()
    return (
        <div className="Input">
            <input ref={input} type="text" id="add-language-id"/>
            <button onClick={() => props.added(input)}>Add</button>
        </div>
    )
}

export default Input
import React, { Component } from 'react';
import './App.css';
import Vote from './Vote/Vote'
import Input from './Input/Input'

class App extends Component {

  state = {
    languages: [
    ]
  }

  voteHandler = (index) => {
    const languages = [...this.state.languages]
    languages[index].VoteCount += 1
    this.setState({
      languages: languages
    })
  }

  addLaguageHandler = (inputElement) => {
    // const languageName = document.getElementById('add-language-id').value
    const languageName = inputElement.current.value

    const language = {
      name: languageName,
      VoteCount: 0
    }
    const languages = [...this.state.languages]
    languages.push(language)
    this.setState({
      languages: languages
    })
  }

  render() {
    const languages = this.state.languages.map((language, index) => {
      return <Vote name={language.name} VoteCount={language.VoteCount} clicked={() => this.voteHandler(index)} />
    })
    return (
      <div className="App">
        <h1>Vote Your Language!</h1>
        <Input added={this.addLaguageHandler} />
        {languages}
      </div>
    );
  }
}

export default App;
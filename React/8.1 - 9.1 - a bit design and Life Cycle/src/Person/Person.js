import React, { useEffect } from 'react'
import styles from './Person.css'

const person = props => {

    console.log('Person.js --- render');

    useEffect(() => {
        console.log('Person.js --- useEffect (alternative - componentDidMount)');

        return () => {
            console.log('Person.js --- useEffect (alternative - componentWillUnmount)');
        }
    }, [])

    return (
        <div className={styles.person}>
            <button onClick={props.clicked} className={styles.Button}>Sil</button>
            <p>I am {props.name} and I am {props.age} years old</p>
            <p>Hobby: {props.children}</p>
            <input type="text" onChange={props.changed} value={props.name} />
        </div>
    )
}

export default person

//state less companent - dummy component - presentational component
import React, { Component } from 'react';
import styles from './App.css';
import Person from './Person/Person'

class App extends Component {

  constructor(props) {
    super(props)
    console.log('App.js --- constructor');
  }

  state = {
    persons: [
      { id: "a1", name: 'Alish', age: "28" },
      { id: "a2", name: 'Fariz', age: "27" },
      { id: "a3", name: 'Meqsed', age: "23" },
    ],
    showPerson: true
  }

  static getDerivedStateFromProps(props, state) {
    console.log('App.js --- getDerivedStateFromProps');
    return state
  }

  componentDidMount() {
    console.log('App.js --- componentDidMount');
  }

  // componentWillMount() {
  //   console.log('App.js --- componentWillount');
  // }

  shouldComponentUpdate(nextProps, nextState) {
    console.log('App.js --- shouldComponentUpdate');
    if (nextState.persons[0].name == 'X')
      return false
    else
      return true
  }

  componentDidUpdate() {
    console.log('App.js --- componentDidUpdate');
  }

  togglePersonHandler = () => {
    this.setState({
      showPerson: !this.state.showPerson
    })
  }

  deletePersonhandler = (personIndex) => {
    const myPersons = [...this.state.persons]
    myPersons.splice(personIndex, 1)

    this.setState({ persons: myPersons })
  }

  nameChangedHandler = (event, personId) => {
    const personIndex = this.state.persons.findIndex(person => {
      return person.id === personId
    })

    const person = { ...this.state.persons[personIndex] }

    person.name = event.target.value

    const myPersons = [...this.state.persons]
    myPersons[personIndex] = person

    this.setState({ persons: myPersons })
  }

  render() {
    console.log('App.js --- render');
    let persons = null

    if (this.state.showPerson) {
      persons = (
        <div>
          {
            this.state.persons.map((person, index) => {
              return <Person
                clicked={() => this.deletePersonhandler(index)}
                changed={(event) => this.nameChangedHandler(event, person.id)}
                name={person.name}
                age={person.age}
                key={person.id}
              />
            })
          }
        </div>
      )
    }
    return (
      <div className={styles.App}>
        <h2>Hi</h2>
        <button onClick={this.togglePersonHandler} className={styles.Button}>Toggle Person</button>
        {persons}
      </div>
    );
  }
}

export default App;

// statefull component, main component
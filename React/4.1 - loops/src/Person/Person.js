import React from 'react'
import './Person.css'

const person = props => {
    return (
        <div className="person">
            <button onClick={props.clicked}>Sil</button>
            <p>I am {props.name} and I am {props.age} years old</p>
            <p>Hobby: {props.children}</p>
            <input type="text" onChange={props.changed}/>
        </div>
    )
}

export default person

//state less companent - dummy component - presentational component
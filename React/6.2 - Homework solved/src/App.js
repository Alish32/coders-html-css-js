import React, { Component } from 'react';
import './App.css';
import Input from './Input/Input';
import Tasks from './Tasks/Tasks';

class App extends Component {

  state = {
    tasks: ['Homework', 'Reading'],
    taskName: '',
    taskIndex: null,
    addOrEdit: 'add'
  }

  taskDeleteHandler = (index) => {
    console.log(index);
    const tasks = [...this.state.tasks]
    tasks.splice(index, 1)

    this.setState({
      tasks: tasks
    })
  }

  taskAddHandler = () => {
    const task = this.state.taskName
    const taskIndex = this.state.taskIndex
    const tasks = [...this.state.tasks]
    if (this.state.addOrEdit === 'add')
      tasks.push(task)
    else
      tasks[taskIndex] = task
    this.setState({
      tasks: tasks,
      addOrEdit: 'add',
      taskName: '',
      taskIndex: null
    })
  }

  changeTaskNameHandler = (event) => {
    this.setState({
      taskName: event.target.value
    })
  }

  editTaskHandler = (index) => {
    this.setState({
      taskName: this.state.tasks[index],
      addOrEdit: 'edit',
      taskIndex: index
    })
  }

  render() {
    let taskElements = 'Bosdur'
    if (this.state.tasks.length > 0)
      taskElements = this.state.tasks.map((e, index) => {
        return <Tasks task={e} deleted={() => this.taskDeleteHandler(index)} key={index} edited={() => this.editTaskHandler(index)} />
      })

    return (
      <div className="App">
        <Input added={(event) => this.taskAddHandler(event)} changed={this.changeTaskNameHandler} taskName={this.state.taskName} />
        <div>
          {taskElements}
        </div>
      </div>
    );
  }
}

export default App;
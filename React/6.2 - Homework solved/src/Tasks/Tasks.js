import React from 'react'
import './Tasks.css'

const Tasks = (props) => {
    return (
        <div>
            <div className="Task" >
                <p>{props.task}</p>
                <button onClick={props.deleted}>Completed</button>
                <button onClick={props.edited}>Edit</button>
            </div>
        </div>
    )
}

export default Tasks
import React from 'react'

const Input = (props) => {

    return (
        <div>
            <input type="text" id="asdf" value={props.taskName} onChange={props.changed}/>
            <button onClick={props.added}>Add</button>
        </div>
    )
}

export default Input
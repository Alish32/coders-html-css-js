// if(confirm('gsajsagh')){
//     console.log('Okay pressed');
// } else {
//     console.log('cancel pressed');
// }

// alert('Adnizi daxil edin')

// const name = prompt('Adinizi daxil edin', 'Asdfg')

// console.log(name);

const topla = (a, b) => {
    return a + b
}

const randonTopla = (a, b) => { // impure function
    return a + b + Math.random()
}

let evvelkiNetice = 0
const arr = ['a', 'b']

const cixma = (a, b) => { // impure function
    const ferq = a - b
    evvelkiNetice = ferq
    arr.push('c')
    return ferq
}

console.log(topla(3, 6));
console.log(topla(3, 6));
console.log(topla(3, 6));
console.log(topla(3, 6));
console.log(topla(3, 6));

console.log(randonTopla(3, 6));
console.log(randonTopla(3, 6));
console.log(randonTopla(3, 6));

console.log(cixma(9, 6));
console.log(cixma(9, 6));
console.log(cixma(9, 6));
console.log(cixma(9, 6));

// 2, 3 => 2 * 2* 2

// const powerOf = (x, n) => {
//     let result = 1

//     for(let i = 0; i < n; i++) {
//         result = result * x
//     }

//     return result
// }

// console.log(powerOf(2, 3));

// console.log(Math.pow(2, 3));

// 2, 3
// const powerOf = (x, n) => {
//     if (n == 1)
//         return x

//     return x * powerOf(x, n - 1)
// }

const powerOf = (x, n) => n === 1 ? x : x * powerOf(x, n - 1)

console.log(powerOf(2, 3));


// 1.      powerOf(2, 3) => 2 * powerOf(2, 2) => 2 * 4 => 8

// 2.      powerOf(2, 2) =>  2 * powerOf(2, 1) => 2 * 2 => 4

// 3.      powerOf(2, 1) => 2



// 1*2 * 2 * 2 = 8






const person = {
    name: 'Alish',
    friends: [
        {
            name: 'Orxan',
        },
        {
            name: 'Emin',
            friends: [
                {
                    name: 'Sabir'
                },
                {
                    name: 'Pervin',
                    friends: [
                        {
                            name: 'Xeyal',
                            friends: [
                                {
                                    name: 'Cavad'
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            name: 'Elcin'
        }
    ]
}

const getFriendsName = (person) => {
    const collectedNames = []

    if (!person.friends) {
        return []
    }

    for (const friend of person.friends) {
        collectedNames.push(friend.name)
        collectedNames.push(...getFriendsName(friend))
    }

    return collectedNames
}

console.log(getFriendsName(person));
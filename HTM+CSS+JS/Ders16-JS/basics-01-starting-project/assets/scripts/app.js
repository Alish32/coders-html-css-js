//primitive -- stringler, number, boolen, null, undefined, Symbol

let ad = 'max'
let basqaAd = ad
ad = 'Max2'
console.log(basqaAd);

let num1 = 6
let num2 = num1
num1 = 1
console.log(num2);


//reference -- object, array

let hobbies = ['sport']
let yeniHobbies = [...hobbies]

// hobbies.push('cooking')
yeniHobbies.push('cooking')
// console.log(yeniHobbies);
console.log(yeniHobbies);
console.log(hobbies);


let person = {
    age: 30
}

let anotherPerson = { ...person }

anotherPerson.age = 32

console.log(person);
console.log(anotherPerson);


///////////////
// primitive tip beraberlik
const a = 6
const b = 6

console.log(a === b); // => true

const ad1 = 'Alish'
const ad2 = 'Alish'

console.log(ad1 === ad2);

// reverence tip beraberlik

const person1 = { age: 30 }
const person2 = { age: 30 }

console.log(person1 === person2);
console.log("age comparision: ", person1.age === person2.age);

const hb = ['sports']
const hb2 = ['sports']

console.log(hb === hb2);

///////////
// reference type manupilation

const arr = [1, 3, 4]
// arr = [1,3,4,5] // => error
arr.push(5)

console.log(arr);

const p = {
    age: 30
}

p.age = 34

console.log(p);


//////
//Funksiyalar

const button = document.getElementById('button')



function greet() {
    console.log('Salam');
}

const adam = {
    name: 'alish',
    salamla: function greet() {
        console.log('Salam from inside function');
    }
}

greet()
adam.salamla()

const fAdi = function () {
    console.log('Hello from salamla2');
    asd
}

// button.addEventListener('click', function () {
//     alert()
// })

// arrow function
const aaa = (num1, num2) => num1 + num2

const bbb = function (num1, num2) {
    return num1 + num2
}

console.log(aaa(6, 8));
console.log(bbb(6, 8));


// const a = {}
// const b = new Object()

let a12 = 5
let b12 = 4
let c12 = 7

const yeniDeyer = a12 + ', ' + b12 + ', ' + c12
alert(yeniDeyer)

document.write('<br>')
for(let i = 0; i < 5; i++){
    for(let j = 0; j <= i; j++) {
        document.write('*')
    }
    document.write('<br>')
}
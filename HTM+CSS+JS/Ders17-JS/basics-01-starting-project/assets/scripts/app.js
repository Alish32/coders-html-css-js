// const topla = (num1, num2 = 0) => {
//     console.log(num1 + num2);
// }

// topla(5, 9)
// topla(5)

// // console.log(5 + undefined);


// const hamisiniTopla = (...numbers) => {
//     let cem = 0
//     for(let i of numbers) {
//         cem += i
//     }
//     return cem
// }

// // hamisiniTopla(4, 5, 6, 3) + 5

// document.write(hamisiniTopla(4, 5, 6, 3) + 5)

// console.log('//////////////');
// ///// rest parameters

// const ex = (deyer1, deyer2, ...qalandeyerler) => {
//     console.log("deyer-1: ", deyer1);
//     console.log("deyer-2: ", deyer2);
//     console.log("qalanlar: ", qalandeyerler);
// }

// ex(2,3,4,5, 8, 9, 10)

// // spread operator
// const arr1 = [2, 5, 7]
// const arr2 = [6, 3, 9]

// const bitismisarray = [...arr1, ...arr2]

// console.log(bitismisarray);
// // console.log(arr1.concat(arr2));

// /////////////////////////////////////////
// document.write('<br>')

// function ekrandaGoster(text) {
//     document.write(text)
// }
// function ekrandaGoster2(text) {
//     document.write("salam " + text)
// }

// function hesablayici(num1, num2, cb) {
//     let cem = num1 + num2
//     cb(cem)
// }

// hesablayici(3, 5, ekrandaGoster) // 8

// //////////////////////////////////////////

// const btn = document.getElementById('button')

// const topla3 = (num1, num2 = 0) => {
//     console.log('Topla3-un neticesi: ',num1 + num2);
// }

// // btn.addEventListener('click', topla3(4, 5)) // birbasa isleyecek
// // btn.addEventListener('click', topla3.bind(this, 4, 5))
// btn.addEventListener('click', () => topla3(4, 5))


// // alert()

const h1 = document.querySelector('h1')
const rengDeyis = document.querySelector('button')

console.log(h1.textContent);

h1.textContent = 'Yeni text'
h1.className = 'title'

rengDeyis.addEventListener('click', () => {
    h1.style.color = 'white'
    h1.style.backgroundColor = 'blue'

    const myLi = document.querySelector('li:last-of-type')
    myLi.textContent = 'Deyisilmis text'
    myLi.style.backgroundColor = 'red'
})
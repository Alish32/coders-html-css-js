const images = ['img_nature_wide.jpg', 'img_mountains_wide.jpg', 'img_snow_wide.jpg']

let indikiShekilIndex = 0

const novbetiShekil = () => {
    indikiShekilIndex++
    if (indikiShekilIndex >= images.length)
        indikiShekilIndex = 0
    document.getElementById('myImageDiv').innerHTML = `<img src="${images[indikiShekilIndex]}" />`
    console.log(indikiShekilIndex);
}

const evvelkiShekil = () => {
    indikiShekilIndex--
    if (indikiShekilIndex < 0)
        indikiShekilIndex = images.length - 1
    document.getElementById('myImageDiv').innerHTML = `<img src="${images[indikiShekilIndex]}" />`
    console.log(indikiShekilIndex);
}
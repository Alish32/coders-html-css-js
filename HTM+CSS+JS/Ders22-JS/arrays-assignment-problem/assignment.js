const numbers = [-1, -22, -22, -100, -4, -5, -6]

const numBoyuk5 = numbers.filter(val => val > 5) // 1.ci helli

// [{num: 1}, {num: -22}]

// const mappedNumbers = numbers.map(val => { // 2.ci helli
//     return { num: val }
// })

const mappedNumbers = numbers.map(val => ({ num: val }) ) // 2.ci helli - qisa yazilis


const multiplication = numbers.reduce((evvelkiVal, indikiVal) => evvelkiVal * indikiVal, 1) // 3. helli


console.log(numBoyuk5);
console.log(mappedNumbers);
console.log(multiplication);


// console.log(Math.max(...numbers));

const maxTap = (nums) => {
    let curMax = 0
    for(const num of numbers) {
        if(num > curMax)
        curMax = num
    }
    return curMax
}

console.log(maxTap(numbers));

const sortedArray = numbers.sort((a, b) => {
    if(a>b) {
        return 1
    } else if(a==b) {
        return 0
    } else {
        return -1
    }
})

// console.log(sortedArray);
const sorted = sortedArray.reverse()
console.log(sorted[0]);
// console.log(sortedArray[numbers.length - 1]);

[7, 5, 99]
const maxAndMinTap = (nums) => {
    let curMax = nums[0]
    let curMin = nums[0]
    for(const num of nums) {
        if(num > curMax)
        curMax = num // 99
        if(num < curMin)
        curMin = num // 5
    }
    return [curMax, curMin]
}

// const min = maxAndMinTap(numbers)[0]
// const max = maxAndMinTap(numbers)[1]

const [max, min] = maxAndMinTap(numbers)
console.log(min);
console.log(max);

const uniques = new Set(numbers)
console.log(uniques);
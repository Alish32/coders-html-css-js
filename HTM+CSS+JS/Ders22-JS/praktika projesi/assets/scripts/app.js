const button = document.querySelector('header button')
const backdrop = document.getElementById('backdrop')
const addModal = document.getElementById('add-modal')
const cancelBtn = document.getElementById('cancel')
const addBtn = document.getElementById('add')
const inputElements = document.querySelectorAll('input')
const myDiv = document.getElementById('movie-list')

const movies = []
/////////////// ---- functions

const modalHandler = () => {
    addModal.classList.toggle('visible')
    backdrop.classList.toggle('visible')
}

const clearMovieInputs = () => {
    inputElements[0].value = ''
    inputElements[1].value = ''
    inputElements[2].value = ''
}

const addMovies = (id, title, url, rating) => {
    const myListElement = document.createElement('div')
    myListElement.innerHTML = `
        <img src="${url}" />
        <h2>${title}</h2>
        <p>${rating}/5</p>
        <button>Sil</button>
    `

    /* myListElement.addEventListener('click', () =>  removeMovie(id, myListElement) ) */
    myListElement.querySelector('button').addEventListener('click', () =>  removeMovie(id, myListElement))
    myDiv.append(myListElement)

}

const removeMovie = (id, element) => {
    const index = movies.findIndex( movie =>  id == movie.id )
    movies.splice(index, 1)
    element.remove()
    console.log(movies);
}

const validateInputs = () => {
    let titleValue = inputElements[0].value
    let imageUrlValue = inputElements[1].value
    let ratingValue = inputElements[2].value
    const id = Math.random()


    if (titleValue === '' || imageUrlValue === '' || ratingValue === '' || ratingValue < 1 || ratingValue > 5) {
        alert('duzgin doldurulmayib...')
    } else {
        const newMovie = {
            id: id,
            title: titleValue,
            image: imageUrlValue,
            rating: ratingValue
        }

        movies.push(newMovie)
        modalHandler()
        clearMovieInputs()
        addMovies(id, titleValue, imageUrlValue, ratingValue)
    }

    console.log(movies);

}
/////////////// ---- button clicks

button.addEventListener('click', () => {
    modalHandler()
})

backdrop.addEventListener('click', () => {
    modalHandler()
    clearMovieInputs()
})

cancelBtn.addEventListener('click', () => {
    modalHandler()
    clearMovieInputs()
})

addBtn.addEventListener('click', () => {
    validateInputs()
})
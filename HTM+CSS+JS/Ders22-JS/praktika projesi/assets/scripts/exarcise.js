const complexPerson = {
  'first name': 'Max',
  hobbies: ['sports', 'walking', 'swimming'],
  age: 27,
  country: 'Azerbaijan',
  phone: {
    number: '0503453245',
    isMobile: true
  },
  5: 'hello',
  isAdmin: true,
  salamla(){
    console.log('Hello. My country is ', this.country);
  },
  getBoyukText(){
    return this.country.toUpperCase()
  }
 }
/* 
 complexPerson.age = 30
 complexPerson.job = 'Engineer'
 delete complexPerson.country
 console.log(complexPerson.age);
 console.log(complexPerson.job);
 console.log(complexPerson['first name']);
 console.log(complexPerson.phone.number);
 console.log(complexPerson.hobbies[1]);
 console.log(complexPerson['5']);

 complexPerson.salamla()

 console.log(complexPerson); */
 console.log(complexPerson.country);
 complexPerson.salamla()
 console.log(complexPerson.getBoyukText());
// const secim = 'toplama'
// const a = 5
// const b = 7

// let result

// console.log(result);

// if (secim === 'toplama') {
//     result = a + b
// } else if (secim === 'cixma') {
//     result = a - b
// } else if (secim === 'vurma') {
//     result = a * b
// } else if (secim === 'bolme') {
//     result = a / b
// } else {
//     alert('secim duz deyil')
// }

// let netice

// switch (secim) {
//     case 'toplama':
//         netice = a + b
//         break
//     case 'cixma':
//         netice = a - b
//         break
//     case 'vurma':
//         netice = a * b
//         break
//     case 'bolme':
//         netice = a / b
//         break
//     default:
//         netice = 'emeliyyat sehvdir'
// }
// console.log(netice);



// const btn = document.getElementById('button')

// btn.addEventListener('click', () => {
//     const a = document.getElementById('num1').value
//     a = +a
//     const b = +document.getElementById('num2').value
//     const result = a + b
//     console.log(result);
// })

// console.log(arr[0]);
// console.log(arr[1]);
// console.log(arr[2]);

for (let i = 0; i < 10; i++) {
    console.log(i);
}

for (let i = 10; i > 0; i--) {
    console.log(i);
}


// for(let i = 0; i < 10; i++){
//     console.log('Salam' + i);
//     document.write(' Salam <br>');
// }


let arr = [1, 2, 5, 8, 6]

for (let i = 0; i < arr.length; i++) {
    console.log("simple for: " + arr[i]);
}

let index = 0
for (const element of arr) {
    if (index === 2)
        break
    console.log('of for: ' + element);
    index++
}


let arr2 = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
]

// console.log(arr2[0][0]);
// console.log(arr2[0][1]);
// console.log(arr2[0][2]);

// console.log(arr2[1][0]);
// console.log(arr2[1][1]);
// console.log(arr2[1][2]);

index = 0
for (const i of arr2) {
    for (const j of arr2[index]) {
        console.log(j);
    }
}


const object = {
    name: 'Alish',
    age: 27,
    job: 'Engineer',
}

console.log(object.name);
console.log(object['name']);
console.log(object.age);
console.log(object.job);

for (const property in object) {
    console.log(object[property]);
}

const users = [
    {
        name: 'Alish',
        age: 27,
        job: 'Engineer',
    },
    {
        name: 'Orxan',
        age: 21,
        job: 'Doctor',
    },
    {
        name: 'Cavad',
        age: 17,
        job: 'Teacher',
    },
    {
        name: 'Senan',
        age: 21,
        job: 'Doctor',
    },
]

console.log('/////////////');

colDongu: for (const user of users) {
    icDongu: for (const property in user) {
        if (user.job == 'Doctor')
            console.log(user[property]);
    }
}


let say = -5

while (say < 3) {
    console.log('while dongusunun ici');
    say++
}

let j = -5
do {
    console.log('do dongusunun ici');
    j++
} while (j < 3)




let str = 23

console.log(Math.floor(23 / 10), 23 % 10);

while (true) {
    const rasgele = Math.random()
    if (rasgele < 0.5) {
        break
    }
    console.log(rasgele);
}


for (let i = 0; i < 10; i++) {
    if (i == 4) {
        break
    }
    console.log(i);
}

for (let i = 0; i < 10; i++) {
    if (i == 4) {
        continue
    }
    console.log(i);
}
console.log('////////');
for (let i = 0; i < 10; i++) {
    if (i != 4) {
        continue
    }
    console.log(i);
}


coldekiLoop: for (let i = 0; i < 10; i++) {
    console.log('coldeki: ' + i);
    iceridekiLoop: for (let j = 0; j < 10; j++) {
        if (j == 5)
            break coldekiLoop
        console.log('icerideki: ' + j);
    }
}

console.log('////');
coldekiLoop: for (let i = 0; i < 10; i++) {
    console.log('coldeki: ' + i);
    iceridekiLoop: for (let j = 0; j < 10; j++) {
        if (i == 5)
            break
        console.log('icerideki: ' + j);
    }
}
const box = document.getElementById('main-box')

console.dir(box);
console.log(box.offsetTop);
console.log(box.offsetLeft);

console.log(box.clientTop);
console.log(box.clientLeft);

console.log(box.offsetWidth);
console.log(box.offsetHeight);

console.log(box.clientWidth);
console.log(box.clientHeight);

console.log(box.scrollHeight);


const btn1 = document.getElementById('open')
const btn2 = document.getElementById('res')
const srcollButton = document.getElementById('scroll')

const openWindow = () => {
    myWindow = window.open('https://google.com', '', 'width=100, height=100')
}

const resizeWindow = () => {
    // myWindow.resizeTo(250, 250)
    myWindow.resizeBy(100, 150)
    myWindow.focus()
}

btn1.addEventListener('click', () => {
    openWindow()
})

btn2.addEventListener('click', () => {
    resizeWindow()
})

srcollButton.addEventListener('click', () => {
    // window.scroll(0, 100)
    window.scrollBy(0, 100)
    window.scrollTo(0, 100)
})

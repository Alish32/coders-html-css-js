const name = document.getElementById('name')
const email = document.getElementById('email')
const password = document.getElementById('password')
const confirmPassword = document.getElementById('confirm-password')
const error = document.getElementById('error')
const register = (e) => {
    e.preventDefault()
    if(password.value != confirmPassword.value){
        alert('Password eyni deyil')
    } else if(varmi()) {
        alert('Istifadeci movcuddur')
    }
     else {
        const istifadeci = {
            name: name.value,
            email: email.value,
            password: password.value
        }

        let istifadeciler = JSON.parse(localStorage.getItem('istifadecilerKey')) || []
        
        istifadeciler.push(istifadeci)

        localStorage.setItem('istifadecilerKey', JSON.stringify(istifadeciler))
    }
}

const varmi = () => {
    let istifadeciler = JSON.parse(localStorage.getItem('istifadecilerKey')) || []
    let found = false
    for(const istifadeci of istifadeciler) {
        if(email.value == istifadeci.email) {
            found = true
            break
        }
    }

    return found
}
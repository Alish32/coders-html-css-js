let url_string = window.location
let url = new URL(url_string)
let id = url.searchParams.get('id')

const h2 = document.querySelector('h2')
const p = document.querySelector('p')

axios.get('http://jsonplaceholder.typicode.com/posts/' + id)
    .then((response) => {
        h2.innerText = response.data.title
        p.innerText = response.data.body
    })
class Car {
    constructor(brandAdi) {
        this._brandAdi = brandAdi
    }

    get brandAdi() {
        return this._brandAdi
    }

    set brandAdi(ad) {
        this._brandAdi = ad + " - edited"
    }

    persent() {
        return 'I have an ' + this._brandAdi
    }

    static hello(x) {
        return 'Heloo!!!  ' + x.brandAdi
    }
}

class Model extends Car {
    constructor(brandAdi, modelAdi) {
        super(brandAdi)
        this.modelAdi = modelAdi
    }

    show() {
        return this.persent() + ' , it is a ' + this.modelAdi
    }
}


let myCar = new Model('Mercedes', 'W222')
console.log(Car.hello(myCar));
let myCar2 = new Model('Audi', 'Q7')
// myCar.brandAdi = 'Volvo'
myCar.brandAdi = 'Volvo'

console.log(myCar.brandAdi);
console.log(myCar.modelAdi);
console.log(myCar.persent());
console.log(myCar.show());


console.log(myCar2.brandAdi);
console.log(myCar2.modelAdi);
console.log(myCar2.persent());
console.log(myCar2.show());
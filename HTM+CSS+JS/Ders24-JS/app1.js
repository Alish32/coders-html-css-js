const calendar = document.querySelector('input')
const img = document.querySelector('img')
const myP = document.querySelector('p')

const changeImage = (event) => {
    const date = new Date(event.target.value)
    const day = date.getDay()
    if (day == 0 || day == 6) {
        img.src = 'dance.gif'
        myP.innerText = 'Yuppiieee'
        myP.style.color = 'green'
    } else {
        img.src = 'booring.gif'
        myP.innerText = 'ouch'
        myP.style.color = 'red'
    }
}


// const numbers = [40, 100, 1, 5, 25, 10]
// console.log(numbers);

// // numbers.sort( (a, b) => {
// //     if(a > b) {
// //         return 6
// //     } else if (a == b) {
// //         return 0
// //     } else {
// //         return -8
// //     }
// // })

// // numbers.sort ((a, b) => a - b)

// // numbers.sort()

// console.log(numbers);

///////// Dates


// const d = new Date()
// const oldDate = new Date("October 26, 2019")
// const oldDate2 = new Date("October 29, 2019")

// console.log((oldDate2.getTime() - oldDate.getTime()) / 1000 / 60 / 60 / 24);

// // const oldDate = new Date(-100000000)

// console.log(d);
// console.log(oldDate.toDateString());
// console.log(oldDate.toUTCString());
// console.log(oldDate.toISOString());


// console.log(d.getTime());
// console.log(d.getFullYear());
// console.log(d.getMonth());
// console.log(d.getDate());
// console.log(d.getMinutes());
// console.log(d.getSeconds());

// const newYear = new Date()
// newYear.setFullYear(2021)
// console.log(newYear);


const pi = Math.PI

console.log(pi);
console.log(typeof pi);
console.log(Number.MAX_SAFE_INTEGER);
console.log(Number.MIN_SAFE_INTEGER);

console.log(Math.round(4.7));
console.log(Math.round(4.4));

console.log(Math.pow(2, 3));
console.log(Math.sqrt(9));
console.log(Math.abs(-9));
console.log(Math.ceil(4.4));
console.log(Math.floor(4.4));

console.log(Math.min(3, 6, 1, 9, 10));
console.log(Math.max(3, 6, 1, 9, 10));

const rdmNum = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1) + min)
}

console.log(rdmNum(4, 7));